<?php
/**
 * @package    report_cuprogress
 * @copyright  2017 Cornerstone University {@link http://www.cornerstone.edu}
 * @author	   Trevor McCready
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
		/* allows the user to view student attendance */
		'report/cuprogress:view' => array(
				'riskbitmask' => RISK_PERSONAL,
				'captype' => 'read',
				'contextlevel' => CONTEXT_COURSE,
				'archetypes' => array(
					'teacher' => CAP_ALLOW,
            		'editingteacher' => CAP_ALLOW,
            		'manager' => CAP_ALLOW
				),
		),
);