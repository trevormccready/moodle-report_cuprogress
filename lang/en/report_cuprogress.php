<?php
$string['pluginname']	=	'CU Student Progress';
$string['allusers']		=	'All';
$string['contributions'] = 	'Contributions';
$string['cuprogress:view'] = 'View CU Progress Report';
$string['lastcourseaccess'] =	'Last Access';
$string['logdetails'] =	'Access Log';
$string['pagetitle'] = 'Student Progress Report';
