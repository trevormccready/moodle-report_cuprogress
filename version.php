<?php
/**
 * @package		report_cuprogress
 * @copyright	2017 Cornerstone University {@link http://www.cornerstone.edu}
 * @author 		Trevor McCready
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version	=	2017033100;
$plugin->requires	=	2016052302;
$plugin->release	=	'v0.2';
$plugin->maturity	=	MATURITY_ALPHA;
$plugin->component	=	'report_cuprogress';