<?php
/**
 * Local library functions for the CU Progress report
 * 
 * @package    report_cuprogress
 * @copyright  2017 Cornerstone University {@link http://www.cornerstone.edu}
 * @author	   Trevor McCready
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function report_cuprogress_get_group_options($id) {
	$groupsfromdb = groups_get_all_groups($id);
	$groups = array();
	foreach ($groupsfromdb as $key => $value) {
		$groups[$key] = $value->name;
	}
	return $groups;
}
function report_cuprogress_output_action_buttons($id, $group, $querystart, $queryend, $timezone, $url) {
	global $OUTPUT;
	
	$querystartdate = date_format_string($querystart, '%F', $timezone);
	$queryenddate = date_format_string($queryend, '%F', $timezone);
	$html = html_writer::start_tag('div');
	$html .= html_writer::start_tag('form',array('id'=>'cuprogressdatefilter','method'=>'get','action'=>$url));
	$html .= html_writer::nonempty_tag('h5','Report Date Range');
	$html .= html_writer::nonempty_tag('label','From');
	$html .= html_writer::empty_tag('input',array('type'=>'text', 'id'=>'querystartdate','name'=>'querystartdate', 'size'=>'10', 'value'=>$querystartdate, 'title'=>'0:00:00'));
	$html .= html_writer::nonempty_tag('label','To');
 	$html .= html_writer::empty_tag('input',array('type'=>'text', 'id'=>'queryenddate','name'=>'queryenddate', 'size'=>'10', 'value'=>$queryenddate, 'title'=>'23:59:59'));
	$html .= html_writer::empty_tag('input',array('type'=>'hidden', 'name'=>'id', 'value'=>$id));
	$html .= html_writer::empty_tag('input',array('type'=>'submit', 'name'=>'filter', 'value'=>'Update'));
	$html .= html_writer::end_tag('form');
	$html .= html_writer::end_tag('div');
	
	$groups = report_cuprogress_get_group_options($id);
	$groupurl = clone $url;
	$select = new single_select($groupurl, 'group', $groups, $group, array('' => get_string('allusers', 'report_cuprogress')));
	$select->label = get_string('group');
	$html .= html_writer::start_tag('div',array('id'=>'resultsfilterbygroup'));
	$html .= $OUTPUT->render($select);
	$html .= html_writer::end_tag('div');
	return $html;
}

function report_cuprogress_get_last_access($course, $USER) {
	global $DB;
	$lastaccess = $DB->get_field('user_lastaccess', 'timeaccess', array('courseid' => $course->id, 'userid' => $USER->id));
	return $lastaccess;
}
