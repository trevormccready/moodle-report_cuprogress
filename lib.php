<?php
/**
* @package    report_cuprogress
* @copyright  2017 Cornerstone University {@link http://www.cornerstone.edu}
* @author	  Trevor McCready
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

defined('MOODLE_INTERNAL') || die;


/**
 * Callback to verify if the given instance of store is supported by this report or not.
 *
 * @param string $instance store instance.
 *
 * @return bool returns true if the store is supported by the report, false otherwise.
 */
function report_cuprogress_supports_logstore($instance) {
	if ($instance instanceof \core\log\sql_reader) {
		return true;
	}
	return false;
}

/**
 * Function to add link to Course Navigation under Reports
 * 
 * @param unknown $navigation
 * @param unknown $course
 * @param unknown $context
 */
function report_cuprogress_extend_navigation_course($navigation, $course, $context) {
	if (has_capability('report/cuprogress:view', $context)) {
		$url = new moodle_url('/report/cuprogress/index.php', array('id'=>$course->id));
		$navigation->add(get_string('pluginname', 'report_cuprogress'), $url, navigation_node::TYPE_SETTING, null, null, new pix_icon('i/report', ''));
	}
}