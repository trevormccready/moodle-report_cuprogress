$(document).ready(function() {
	$('.section_results').click(function() {
		$(this).parent().find('ul.section_details').toggle();
	});
	$('#atriskstudentsbutton').click(function() {
		$('tr.notflagged').hide();
		$(this).hide();
		$('#allstudentsbutton').show();
	});
	$('#allstudentsbutton').click(function() {
		$('tr.notflagged').show();
		$(this).hide();
		$('#atriskstudentsbutton').show();
	});
	$('#toggleassignmentresults').click(function() {
		if ( $('#toggleassignmentresults').attr('data-visibility') == 'hidden' ) {
			$('ul.section_details').show();
			$('#toggleassignmentresults').attr('data-visibility','shown');
			$('#toggleassignmentresults').text('Hide all assignment results');
		} else {
			$('ul.section_details').hide();
			$('#toggleassignmentresults').attr('data-visibility','hidden');
			$('#toggleassignmentresults').text('Show all assignment results');
		}
	})
});
