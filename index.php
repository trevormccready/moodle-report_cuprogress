<?php
/**
 * @package    report_cuprogress
 * @copyright  2017 Cornerstone University {@link http://www.cornerstone.edu}
 * @author	   Trevor McCready
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require (__DIR__ . '/../../config.php');
require_once ($CFG->libdir . '/adminlib.php');
require_once ($CFG->libdir . '/completionlib.php');
require_once ($CFG->dirroot . '/grade/lib.php');
require_once ($CFG->dirroot . '/grade/report/user/lib.php');
require_once ($CFG->libdir . '/gradelib.php');
require_once ($CFG->libdir . '/modinfolib.php');
require_once (dirname ( __FILE__ ) . '/locallib.php');
$results = null;
$id = required_param ( 'id', PARAM_INT );
$group = optional_param ( 'group', 0, PARAM_INT );
$course = $DB->get_record ( 'course', array (
		'id' => $id 
), '*', MUST_EXIST );

$page = optional_param ( 'page', 0, PARAM_INT );
$perpage = optional_param ( 'perpage', 50, PARAM_INT ); // how many per page
$sort = optional_param ( 'sort', 'lastname', PARAM_ALPHA );
$dir = optional_param ( 'dir', 'ASC', PARAM_ALPHA );
$querystartdate = optional_param ( 'querystartdate', null, PARAM_NOTAGS );
$queryenddate = optional_param ( 'queryenddate', null, PARAM_NOTAGS );
if (! is_null ( $querystartdate )) {
	$querystartdate = strtotime ( $querystartdate );
}
if (! is_null ( $queryenddate )) {
	$queryenddate = strtotime ( $queryenddate . ' 23:59:59' );
}
require_login ( $course );
$context = context_course::instance ( $course->id );
require_capability ( 'report/cuprogress:view', $context );
$PAGE->requires->js ( '/report/cuprogress/javascript/jquery.js', true );
$PAGE->requires->js ( '/report/cuprogress/javascript/jquery-ui.js', true );
$PAGE->requires->js('/report/cuprogress/javascript/cuprogress.js',true);
$PAGE->set_url ( '/report/cuprogress/index.php', array (
		'id' => $id 
) );
$PAGE->set_title ( get_string ( 'pagetitle', 'report_cuprogress' ) . ': ' . $COURSE->fullname );
$returnurl = new moodle_url ( '/course/view.php', array (
		'id' => $id 
) );

$activestudentdisplay = "No active students enrolled";
$today = time();

// Set assignment names ... eventually need to pull these from the module plugin language field
$assignmenttype ['assign'] = 'Assignment';
$assignmenttype ['forum'] = 'Forum';
$assignmenttype ['hsuforum'] = 'Advanced Forum';
$assignmenttype ['quiz'] = 'Quiz';
$assignmenttype ['scorm'] = 'SCORM';
$assignmenttype ['turnitintooltwo'] = 'Turnitin';
// Set html for report values
$result ['unknown'] = html_writer::empty_tag('i',array('class'=>'fa fa-question incomplete'));
$result ['incomplete'] = "-- ";
$result ['fail'] = html_writer::empty_tag('i',array('class'=>'fa fa-exclamation-triangle fail'));
$result ['atrisk'] = html_writer::empty_tag('i',array('class'=>'fa fa-exclamation-circle atrisk'));
$result ['pass'] = html_writer::empty_tag('i',array('class'=>'fa fa-check-circle pass'));
$result ['max'] = html_writer::empty_tag('i',array('class'=>'fa fa-plus-circle max'));

// Prepare the column headings
$columns = array (
		'firstname' => get_string ( 'firstname' ),
		'lastname' => get_string ( 'lastname' ),
		'lastcourseaccess' => get_string ( 'lastcourseaccess', 'report_cuprogress' ) 
);

// Pull list of students
$studentlist = get_role_users ( 5, $context );
$studentids = [ ];
foreach ( $studentlist as $studentuser ) {
	$studentids [] = $studentuser->id;
}

// Process course section and module results
$course_modinfo = get_fast_modinfo ( $COURSE->id );
$course_grades = core_grades_external::get_grades ( $COURSE->id, null, null, $studentids );

$completion = new completion_info($COURSE);

// Setup Course Total column
$course_total = ( object ) $course_grades ['items'] ['course'];
$passing_grade_disclaimer = null;
if ($course_total->grademax == 100) {
	if ($course_total->gradepass > 0) {
		$passing_grade = $course_total->gradepass;
	} else {
		$passing_grade = 70;
		$passing_grade_disclaimer = "Pass => 70";
	}
} elseif ($course_total->grademax > 0) {
	$passing_grade = $course_total->grademax * 0.70;
	$passing_grade_disclaimer = "Pass => 70%";
} else {
	$passing_grade = 0;
	$passing_grade_disclaimer = "Pass Not Set";
}
if ($passing_grade == 70) {
	$atrisk_grade = 77;
} elseif ($passing_grade > 0) {
	$atrisk_grade = $course_total_grademax * 0.77;
} else {
	$atrisk_grade = 0;
}

// Setup Section columns
$graded_items = $course_grades ['items'];
$gradeitems = $DB->get_records('grade_items',array ( 'courseid' => $COURSE->id ),null,'id,gradetype');
$sectionnumber = 0;
$totalsections = count ( $course_modinfo->sections );
foreach ( $course_modinfo->sections as $sectionmodules ) {
	$section = $course_modinfo->get_section_info ( $sectionnumber );
	$sections [] = $section;
	$gradedsectionitems = 0;
	if ($section->section == 0) {
		$columns [$section->section] = 'Course Grade';
		$sectionitems [$section->section] = null;
	} else {
		foreach ( $sectionmodules as $sectionmodule ) {
			if (array_key_exists ( $sectionmodule, $graded_items )) {
				// VERIFY MODULE ITEM IS IN FACT IN THE GRADEBOOK BEFORE ADDING TO $UNIT
				if ($gradeitems[$graded_items[$sectionmodule]['id']]->gradetype > 0) {
					$unit [$sectionnumber] [$sectionmodule] = ( object ) $graded_items [$sectionmodule];
					$gradedsectionitems ++;
				}
			}
			$sectionitems [$section->section] = $gradedsectionitems;
			$columns [$section->section] = $section->name;
		}
	}
	$sectionnumber ++;
}

// Determine and format course dates
$minweeks = count($unit);
$coursedayofweek = date_format_string ( $COURSE->startdate, '%A', $USER->timezone );
$mostrecentcourseduedate = strtotime ( 'last ' . $coursedayofweek );
$weekdate [0] = strtotime ( '-7 days', $COURSE->startdate );
for($x = 1; $x <= $minweeks; $x ++) {
	$weekdate [$x] = strtotime ( '+7 days', $weekdate [$x - 1] );
	$weekstart [$x] = strtotime ( '-6 days', $weekdate [$x] );
	$weeks [$x] = 'Week ' . $x . ' (' . date_format_string ( $weekstart [$x], '%F', $USER->timezone ) . ' to ' . date_format_string ( $weekdate [$x], '%F', $USER->timezone ) . ')';
}
if (!ISSET($COURSE->enddate)) { // PREPARE FOR MOODLE 3.2 WHERE THIS FIELD WILL BE ADDED
	$COURSE->enddate = $weekdate[$minweeks];
}
if ($weekdate[0] < $today && $COURSE->enddate > $today) {
	$courseisactive = 1;
} else {
	$courseisactive = 0;
}
if ($mostrecentcourseduedate < $weekdate [$minweeks] && $mostrecentcourseduedate >= $weekdate [0]) {
	$z = 1;
	foreach ( $weekdate as $classdate ) {
		if ($mostrecentcourseduedate == $classdate) {
			$mostrecentweek = $z;
			break;
		} else {
			$z ++;
		}
	}
	if (isset ( $mostrecentweek )) {
		if (is_null ( $querystartdate )) {
	        $querystartdate = $weekstart [$mostrecentweek];
		}
		if (is_null ( $queryenddate )) {
			$queryenddate = strtotime ( date_format_string ( $weekdate [$mostrecentweek], '%F', $USER->timezone ) . ' 23:59:59' );
		}
	}
} else {
	if (is_null ( $querystartdate )) {
		if ($weekstart [1] >= strtotime ( "now" )) {
			$querystartdate = strtotime ( date ( 'Y-01-01' ) );
		} else {
			$querystartdate = $weekstart [1];
		}
	}
	if (is_null ( $queryenddate )) {
		$queryenddate = strtotime ( "now" );
	}
}
$coursestartdate = 'Course Start Date: ' . date_format_string ( $COURSE->startdate, '%A %B %e, %Y', $USER->timezone );
$datechoices = 'By default, the following date range was used: ' . date_format_string ( $querystartdate, '%F', $USER->timezone ) . ' - ' . date_format_string ( $queryenddate, '%F', $USER->timezone );

// SET SORT COLUMNS
if (! isset ( $columns [$sort] )) {
	$sort = 'lastname';
}
$hcolumns = array ();
foreach ( $columns as $column => $strcolumn ) {
	if ($sort != $column) {
		$columnicon = '';
		if ($column == 'lastmdlaccess') {
			$columndir = 'DESC';
		} else {
			$columndir = 'ASC';
		}
	} else {
		$columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
		if ($column == 'lastcourseaccess') {
			$columnicon = $dir == 'ASC' ? 'up' : 'down';
		} else {
			$columnicon = $dir == 'ASC' ? 'down' : 'up';
		}
		$columnicon = " <img src=\"" . $OUTPUT->pix_url ( 't/' . $columnicon ) . "\" alt=\"\" />";
	}
	//$hcell = new html_table_cell ('<a href="index.php?id='.$COURSE->id.'&amp;sort='.$column.'&amp;dir='.$columndir.'&amp;page='.$page.'&amp;perpage='.$perpage.'">' . $strcolumn . '</a>'.$columnicon);
	// No sorting capabilities...add later
	$hcell = new html_table_cell ( $strcolumn );
	if (is_numeric($column) && $column > 0) {
		$hcell->attributes ['class'] = 'sectioncol';
	}
	$hcolumns [$column] = $hcell;
}
$override = new stdClass ();
$override->firstname = 'firstname';
$override->lastname = 'lastname';
$fullnamelanguage = get_string ( 'fullnamedisplay', '', $override );
if (($CFG->fullnamedisplay == 'firstname lastname') or ($CFG->fullnamedisplay == 'firstname') or ($CFG->fullnamedisplay == 'language' and $fullnamelanguage == 'firstname lastname')) {
	$fullnamedisplay = $hcolumns ['firstname']->text . ' / ' . $hcolumns ['lastname']->text;
} else { // ($CFG->fullnamedisplay == 'language' and $fullnamelanguage == 'lastname firstname')
	$fullnamedisplay = $hcolumns ['lastname']->text . ' / ' . $hcolumns ['firstname']->text;
}

// SETUP RESULTS TABLE
$table = new html_table ();
$table->head = array (
		$fullnamedisplay,
		$hcolumns ['lastcourseaccess']->text,
		$hcolumns [0]->text 
);
$table->headspan = array (
		1,
		1,
		1 
);
$table->colclasses = array (
		'name',
		'lastcourseaccess',
		'grade' 
);
$header_desc_row = new html_table_row ();
$header_desc_row->cells = array (
		null,
		'# of days' 
);
if ($passing_grade_disclaimer) {
	$header_desc_row->cells [] = $passing_grade_disclaimer;
} else {
	$header_desc_row->cells [] = null;
}
foreach ( $unit as $sectioncol => $activities ) {
	$headcell = new html_table_cell ($hcolumns [$sectioncol]->text);
	$headcell->attributes['class'] = $hcolumns[$sectioncol]->attributes['class'];
	$table->head [] = $headcell;
	if ($sections [$sectioncol]->visible) {
		$visibility = "visible";
	} else {
		$visibility = "dimmed";
	}
	if (! empty ( $sectionitems [$sectioncol] )) {
		$table->headspan [] = $sectionitems [$sectioncol];
		if (is_array ( $activities )) {
			$sectionmodulecount = 1;
			foreach ( $unit [$sectioncol] as $moduleitem ) {
				$table->colclasses [] = 'section' . $sectioncol . ' ' . $visibility;
				if (array_key_exists ( $moduleitem->itemmodule, $assignmenttype )) {
					$title = $assignmenttype [$moduleitem->itemmodule] . ': ' . htmlspecialchars_decode ( $moduleitem->name );
				} else {
					$title = htmlspecialchars_decode ( $moduleitem->name );
				}
				if ($moduleitem->gradepass > 0) {
					$title .= " (Max: " . $moduleitem->grademax . " Pass: " . $moduleitem->gradepass . ")";
				} else {
					$title .= " (Max: " . $moduleitem->grademax . ")";
				}
				$assignmenturl = new moodle_url ( '/grade/report/singleview/index.php', [ 
						'id' => $COURSE->id,
						'item' => 'grade',
						'itemid' => $moduleitem->id 
				] );
				$cell = new html_table_cell ( html_writer::link ( $assignmenturl, $OUTPUT->pix_icon ( 'icon', '', $moduleitem->itemmodule, [ 'class' => 'icon' ] ), array ( 'title' => $title, 'target' => '_blank' ) ) );
				if ($sectionmodulecount == 1) {
					$cell->attributes ['class'] = 'section_col_left';
				}
				if ($sectionmodulecount == count($unit[$sectioncol])) {
					$cell->attributes ['class'] = 'section_col_right';
				}
				$header_desc_row->cells [] = $cell;
				$sectionmodulecount++;
			}
		}
	} else {
		$table->headspan [] = 1;
		$table->colclasses [] = 'section' . $sectioncol . ' ' . $visibility;
	}
}
$table->id = 'cuprogress';
$table->attributes ['class'] = 'generaltable';
$table->data = array ();
$header_desc_row->header = 1;
$header_desc_row->attributes ['class'] = 'header';
$table->data [] = $header_desc_row;

// Process the student specific results
$userlist = get_enrolled_users ( $context, '', $group );
$enrolledcount = count ( $userlist );
if ($enrolledcount > 0) {
	$studentcount = count ( $studentlist );
	$facultylist = get_role_users ( 3, $context );
	$facultycount = count ( $facultylist );
	$suspendedcount = 0;
	if ($studentcount > 0) {
		$suspended = get_suspended_userids ( $context );
		foreach ( $studentlist as $student ) {
			$student_course_grade = ( object ) $course_grades ['items'] ['course'] ['grades'] [$student->id];
			$student->total = $student_course_grade;
			if (! in_array ( $student->id, $suspended )) {
				$activestudent = $student;
				$atrisk = 0;
				$failing = 0;
				$row = new html_table_row ();
				
				// Student Name and Contact Column
				$studentinfo = html_writer::link ( new moodle_url ( $CFG->wwwroot . '/user/view.php?id=' . $activestudent->id . '&course=' . $COURSE->id ), fullname ( $activestudent ), array (
						'title' => $activestudent->idnumber 
				) );
				$studentinfo .= html_writer::link ( new moodle_url ( $CFG->wwwroot . '/message/index.php?user2=' . $activestudent->id ), '<i class="fa fa-comment fa-1" aria-hidden="true"></i>', array (
						'title' => 'Send Message' 
				) );
				$studentinfo .= html_writer::link ( new moodle_url ( 'mailto:' . $activestudent->email ), '<i class="fa fa-envelope fa-1" aria-hidden="true"></i>', array (
						'title' => 'Send Email' 
				) );
				$studentname_cell = new html_table_cell ( $studentinfo );
				$row->cells [] = $studentname_cell;
				
				// Student's Last Access to Course
				$midnight = strtotime('today midnight');
				$lastcourseaccess = report_cuprogress_get_last_access($COURSE,$activestudent);
				$timediff = time() - $lastcourseaccess;
                $cell_class = '';
				if($timediff < 86400) {
					if ($lastcourseaccess > $midnight) {
						$sincelastaccess = html_writer::tag('span','Today');
					} else {
						$sincelastaccess = html_writer::tag('span','Yesterday');
					}
				} else {
					if ($courseisactive && round($timediff / 86400) > 3) {
						$days = html_writer::tag('span',round($timediff / 86400)." days");
						if ($courseisactive && round($timediff / 86400) > 7) { // Hasn't access the course in the past 7 days
							$sincelastaccess = html_writer::tag('i',$days,array('class'=>'fa fa-exclamation-triangle'));
							$cell_class = 'absent alert alert-error';
							$atrisk = 1;
						} else { // Hasn't accessed the course in the past 3 days but less than 7 days
							$sincelastaccess = html_writer::tag('i',$days,array('class'=>'fa fa-exclamation-circle'));
							$cell_class = 'atrisk alert';
							$atrisk = 1;
						}
        		    } else {
                        if ( $lastcourseaccess ) {
    						$sincelastaccess = html_writer::tag('span',round($timediff / 86400)." days");
                        } else {
                            $sincelastaccess = html_writer::tag('i'," Never",array('class'=>'fa fa-exclamation-triangle'));
                            $cell_class = 'absent alert alert-error';
                            $atrisk = 1;
                        }
					}
				}
				$lastaccess = html_writer::link ( new moodle_url ( $CFG->wwwroot . '/report/log/index.php?chooselog=1&showuser=1&edulevel=2&id=' . $COURSE->id . '&user=' . $activestudent->id ), $sincelastaccess, array (
						'target' => 'moodlelog','title' => date("Y-m-d H:i:s",$lastcourseaccess) 
				) );
				$lastaccess_cell = new html_table_cell ( $lastaccess );
				$lastaccess_cell->attributes ['class'] = $cell_class;
				$row->cells [] = $lastaccess_cell;
				// Course Total and Section Progress
				$result_total = html_writer::tag("span",$activestudent->total->str_grade,array('class'=>'student_grade'));
				$sectionnumber = 0;
				foreach ( $sections as $resultcol ) {
					$cell_class = "";
					if ($sectionnumber == 0) {
						// Course Total
						if ($activestudent->total->grade >= 100) {
							$resultstatus = 'max';
						} elseif ($passing_grade <= $activestudent->total->grade) {
							$resultstatus = "pass";
							if ($atrisk_grade > $activestudent->total->grade) {
								$resultstatus = 'atrisk';
								$atrisk = 1;
								$cell_class = 'alert';
							}
						} else {
							if($activestudent->total->grade == 0) {
								$resultstatus = "incomplete";
							} else {
								$resultstatus = 'fail';
								$cell_class = 'alert alert-error';
							}
							$atrisk = 1;
							$failing = 1;
						}
						$student_course_total = html_writer::link(new moodle_url ( $CFG->wwwroot . '/grade/report/singleview/index.php?item=user&id=' . $COURSE->id . '&itemid=' . $activestudent->id ), $result[$resultstatus].$result_total, array( 'target' => '_blank', 'title' => 'Gradebook Single Student View' ));
						$cell = new html_table_cell ( $student_course_total );
						$cell->attributes ['class'] = $resultstatus .' '. $cell_class;
						$row->cells [] = $cell;
					} else {
						// Section results
						if (array_key_exists ( $sectionnumber, $unit )) {
							$sectionmodulecount = 1;
							$sectionresultsummary = "incomplete";
							$cell_details = html_writer::start_tag('ul', array( 'class' => 'section_details list-inline', 'style' => 'display: none;' ));
							$cell_details_width = 100 / count($unit[$sectionnumber]);
							$assignment_status = [];
							foreach ( $unit [$sectionnumber] as $moduleitem ) {
								// Loop through each assignment item
								$cm = $course_modinfo->get_cm($moduleitem->activityid);
								$cell_class = "unknown";
								$studentgrade = $moduleitem->grades [$activestudent->id];
								// Check completion tracking status
								$activitycomplete = 'U';
								if ($completion->is_enabled($cm) == 2) { // AUTOMATIC COMPLETION TRACKING
									$activitycompletion = $completion->get_data($cm,false,$activestudent->id);
									if ($activitycompletion->completionstate) { // MARKED AS COMPLETE
										$activitycomplete = 'C';
									} else {
										$activitycomplete = 'I';
									}
								} elseif ($completion->is_enabled($cm) == 1) { // MANUAL COMPLETION TRACKING
									$activitycompletion = $completion->get_data($cm,false,$activestudent->id);
									if ($activitycompletion->completionstate) { // MANUALLY MARKED COMPLETE
										$activitycomplete = 'M';
									}
								}
								if ($activitycomplete <> "C") {
									// SINCE ACTIVITY COMPLETION WASN'T AUTOMATICALLY TRIGGERED CHECK FOR SUBMISSIONS OR POSTS
									$submission = new stdClass();
									switch($moduleitem->itemmodule) {
										case 'assign':
                                            $assignment_dropbox = $DB->get_record('assign',array('id'=>$moduleitem->iteminstance,'course'=>$COURSE->id),'id,duedate',IGNORE_MISSING);
                                            $assignment_submission = $DB->get_record('assign_submission',array('assignment'=>$moduleitem->iteminstance,'userid'=>$activestudent->id,'status'=>'submitted','latest'=>'1'),'id,timecreated,status,attemptnumber',IGNORE_MISSING);
                                            if ( !$assignment_submission ) {
                                                if ($assignment_dropbox->duedate < time() ) {
                                                    $activitycomplete = 'I';
                                                } else {
                                                    $activitycomplete = 'N';
                                                }
                                            } else {
                                                if ($assignment_dropbox->duedate < $assignment_submission->timecreated) {
                                                    $activitycomplete = 'L';
                                                } else {
                                                    $activitycomplete = 'S';
                                                }
                                            }
											break;
										case 'hsuforum':
                                            $hsuforum_threads = $DB->count_records('hsuforum_discussions',array('forum'=>$moduleitem->iteminstance,'course'=>$COURSE->id,'userid'=>$activestudent->id));
                                            $hsuforum_posts_select = "SELECT COUNT(fp.id) FROM {hsuforum_posts} AS fp INNER JOIN {hsuforum_discussions} AS fd ON fd.id = fp.discussion ";
                                            $hsuforum_posts_where = " WHERE fd.forum = ".$moduleitem->iteminstance." AND fp.userid = ".$activestudent->id." AND fp.parent > 0 ";
                                            $hsuforum_posts = $DB->count_records_sql($hsuforum_posts_select.$hsuforum_posts_where);
                                            if ( $hsuforum_threads == 0 && $hsuforum_posts == 0 ) {
                                                $activitycomplete = 'N';
                                            } else {
                                                $activitycomplete = 'S';
                                            }
                                            break;
										case 'quiz':
											break;
										case 'turnitintooltwo':
											$turnitintooltwo = $DB->get_record('turnitintooltwo',array('id'=>$moduleitem->iteminstance,'course'=>$COURSE->id),'id,numparts',IGNORE_MISSING);
											if($turnitintooltwo->numparts == 1) {
												$submission_part = $DB->get_record('turnitintooltwo_parts',array('turnitintooltwoid'=>$turnitintooltwo->id),'id,dtdue,submitted',IGNORE_MISSING);
												$submission = $DB->get_record('turnitintooltwo_submissions',array('userid'=>$activestudent->id, 'turnitintooltwoid'=>$turnitintooltwo->id, 'submission_part'=>$submission_part->id),'id,submission_objectid,submission_modified', IGNORE_MISSING);
												if ( !$submission ) {
													if ( $submission_part->dtdue < time() ) { // No submission and it is late
														$activitycomplete = 'I'; 
													} else { // No submission but the due date has not passed yet, so its status is unknown
														$activitycomplete = 'N'; 
													}
												} else {
													if ( $submission_part->dtdue < $submission->submission_modified ) { // Submission exists but it was late
														$activitycomplete = 'L';
													} else {
														$activitycomplete = 'S';
													}
												}
											}
											break;
										default:
											break;
									}
								}
								// Check grade
								if (ISSET ($studentgrade['grade'])) { // ITEM HAS BEEN GRADED
									if( $studentgrade['grade'] == 0 ) {
										switch ($activitycomplete) {
											case 'C':
												$assignment_status[] = 0;
												$studentscore = '0';
												break;
											case 'I':
												$assignment_status[] = 2;
												$studentscore = '0 (I)';
												break;
											case 'L':
												$assignment_status[] = 0;
												$studentscore = '0 (L)';
												break;
											case 'M':
												$assignment_status[] = 0;
												$studentscore = '0';
												break;
											case 'N':
												$assignment_status[] = 0;
												$studentscore = '0?';
												break;
											case 'S':
												$assignment_status[] = 0;
												$studentscore = '0';
												break;
											default:
												$assignment_status[] = 0;
												$studentscore = '0';
												break;
										}
									} else { // ITEM HAS A SCORE OF GREATER THAN 0									
										if ($moduleitem->gradepass == 0) {
											$passgrade = $moduleitem->grademax * .7;
										} else {
											$passgrade = $moduleitem->gradepass;
										}
										if ($studentgrade['grade'] < $passgrade) {
											$assignment_status[] = 0;
										} elseif ($studentgrade['grade'] <= $moduleitem->grademax*.77) {
											$assignment_status[] = 1;
										} elseif ($studentgrade['grade'] < $moduleitem->grademax) {
											$assignment_status[] = 6;
										} else {
											$assignment_status[] = 7;
										}
										$studentscore = round ( $studentgrade ['grade'], 2 );
									}
								} else { // Item has not been graded
									switch ($activitycomplete) {
										case 'C':
											$assignment_status[] = 3;
											$studentscore = 'C'; // Completed but not graded
											break;
										case 'I':
											$assignment_status[] = 2;
											$studentscore = 'I'; // Incomplete and not graded
											break;
										case 'L':
											$assignment_status[] = 3;
											$studentscore = 'L'; // Late but not graded
											break;
										case 'M':
											$assignment_status[] = 3;
											$studentscore = '??'; // Manually marked as complete but not graded
											break;
										case 'N':
											$assignment_status[] = 5;
											$studentscore = '--'; // No submission, but due date hasn't arrived and it hasn't been graded
											break;
										case 'S':
											$assignment_status[] = 4;
											$studentscore = 'S'; // Submission found but not graded yet
											break;
										case 'U':
											$assignment_status[] = 5;
											$studentscore = 'U'; // Status is unknown
											break;
									}
								}
								switch(end($assignment_status)) {
									case 0:
										$cell_class = "fail";
										$studentscore .= '!';
										break;
									case 1:
										$cell_class = "atrisk";
										$studentscore .= '~';
										break;
									case 2:
										$cell_class = "incomplete";
										break;
									case 3:
										$cell_class = "ungraded"; 
										break;
									case 4:
										$cell_class = "";
										break;
                                    case 5:
                                        $cell_class = "";
                                        break;
									case 6:
										$cell_class = "pass";
										break;
									case 7:
										$cell_class = "max";
										break;
								}
								if ($studentgrade['overridden'] > 0) {
									$cell_class .= ' overridden';
								}
								$cell_class .= ' instance_' . $moduleitem->id;
								switch($moduleitem->itemmodule) {
									case 'assign':
										$assignmentgraderlink = new moodle_url ( $CFG->wwwroot . '/grade/report/singleview/index.php?id='. $COURSE->id . '&item=user&itemid=' . $activestudent->id );
										$studentscoredisplay = html_writer::link($assignmentgraderlink, $studentscore, array( 'target' => '_blank', 'title' => 'View assignment grader for this student / assignment' ));
										break;
									case 'hsuforum':
											$assignmentcontextid = $DB->get_record('context', array('instanceid' => $moduleitem->activityid, 'contextlevel' => '70'), 'id');
											$grading_area = $DB->get_record('grading_areas', array('contextid' => $assignmentcontextid->id), 'id');
                                            if ($grading_area) { // If the faculty does not use the CU grader at all, then there won't be any record in the database, so we should just display the score without a link
    											$assignmentgraderlink = new moodle_url ( $CFG->wwwroot . '/grade/report/singleview/index.php?id='. $COURSE->id . '&item=user&itemid=' . $activestudent->id );
                                                $studentscoredisplay = html_writer::link($assignmentgraderlink, $studentscore, array( 'target' => '_blank', 'title' => 'View Grade REport for this student' ));
                                            } else {
                                                $assignmentgraderlink = new moodle_url ( $CFG->wwwroot . '/grade/report/singleview/index.php?id='. $COURSE->id . '&item=grade&itemid=' . $moduleitem->activityid );
                                                $studentscoredisplay = html_writer::tag('span', $studentscore, array ( 'style' => 'color: #999;' ));
                                            }
											break;
									case 'quiz':
										$assignmentgraderlink = new moodle_url ( $CFG->wwwroot . '/mod/quiz/report.php?mode=overview&id=' . $moduleitem->activityid );
										$studentscoredisplay = html_writer::link($assignmentgraderlink, $studentscore, array( 'target' => '_blank', 'title' => 'View all attempts for this quiz' ));
										break;
									case 'turnitintooltwo':
										$assignmentgraderlink = new moodle_url ( $CFG->wwwroot . '/mod/turnitintooltwo/view.php?id=' . $moduleitem->activityid );
										$studentscoredisplay = html_writer::link($assignmentgraderlink, $studentscore, array( 'target' => '_blank', 'title' => 'View all submissions for this assignment' ));
										break;
									default:
										$studentscoredisplay = html_writer::tag('span', $studentscore, array ( 'style' => 'color: inherit;' ));
										break;
								}
								$cell_details .= html_writer::tag ( 'li', $studentscoredisplay, array ( 'data-score' => $studentscore, 'class' => $cell_class, 'style' => 'width: '.$cell_details_width.'%;' ) );
								$sectionmodulecount++;
							} // Finish looping through this section's assignments
							$cell_details .= html_writer::end_tag('ul');
							$sectionresultsummary = min($assignment_status);
							$studentresult = "";
							$completionstatus = '';
							switch($sectionresultsummary) {
								case 0: //fail
									$failed = 0;
									$activityincompletes = 0;
									foreach ($assignment_status as $status) {
										if ($status == 0) {
											$failed++;
										} elseif ($status == 2) {
											$activityincompletes++;
										}
									}
									if ($failed && $activityincompletes) {
										$completionstatus = $failed+$activityincompletes.'/'.count($assignment_status).' failed or incomplete';
									} elseif ($failed) {
										$completionstatus = $failed.'/'.count($assignment_status).' failing scores';
									}
									$studentresult = html_writer::tag('i',null,array('class'=>'fa fa-lg fa-exclamation-triangle fail', 'title' => $completionstatus));
									$atrisk = 1;
									$cell_class = 'alert alert-error';
									break;
								case 1: //atrisk
									$activityatrisk = 0;
									$activityincompletes = 0;
									foreach ($assignment_status as $status) {
										if ($status == 1) {
											$activityatrisk++;
										} elseif ($status == 2) {
											$activityincompletes++;
										}
									}
									if ($activityatrisk && $activityincompletes) {
										$completionstatus = $activityatrisk+$activityincompletes.' of '.count($assignment_status).' low passing or incomplete';
									} else {
										$completionstatus = $activityatrisk.' of '.count($assignment_status).' low passing scores';
									}
									$studentresult = html_writer::tag('i',null,array('class'=>'fa fa-lg fa-exclamation-circle atrisk', 'title' => $completionstatus));
									$atrisk = 1;
									$cell_class = 'alert';
									break;
								case 2: //incomplete
									$incomplete = 0;
									foreach ($assignment_status as $status) {
										if ($status == 2) {
											$incomplete++;
										}
									}
									$completionstatus = $incomplete.' of '.count($assignment_status).' incomplete';
									if ( count(array_unique($assignment_status)) === 1 ) {
										$studentresult = html_writer::tag('span','--');;
										$cell_class = null;
									} else {
										$studentresult = html_writer::tag('i',null,array('class'=>'fa fa-lg fa-exclamation atrisk', 'title' => $completionstatus));
										$cell_class = 'alert';
									}
									break;
								case 3: //ungraded
									$studentresult = html_writer::tag('i',null,array('class'=>'fa fa-question ungraded'));
									$cell_class = null;
									break;
                                case 4: //activity complete awaiting grade
                                    if ( count(array_unique($assignment_status)) === 1 ) {
                                        $studentresult = html_writer::tag('span','S');
                                        $cell_class = null;
                                    } else {
                                        $studentresult = html_writer::tag('i',null,array('class'=>'fa fa-question unknown'));
                                        $cell_class = null;
                                    }
                                    break;
								case 5: //unknown
									if ( count(array_unique($assignment_status)) === 1 ) {
										$studentresult = html_writer::tag('span','--');
										$cell_class = null;
									} else {
										$studentresult = html_writer::tag('i',null,array('class'=>'fa fa-lg fa-question unknown'));
										$cell_class = 'alert';
									}
									break;
                                case 6: //pass
									$studentresult = html_writer::tag('i',$completionstatus,array('class'=>'fa fa-check-circle pass'));
									$cell_class = null;
									break;
								case 7: //max
									$completionstatus = html_writer::tag('span','');
									$studentresult = html_writer::tag('i',$completionstatus,array('class'=>'fa fa-plus-circle max'));
									$cell_class = null;
									break;		
							}
							$sectionsummary = html_writer::tag('div',$studentresult,array( 'class' => 'section_summary' ));
							$cell = new html_table_cell ( $sectionsummary.$cell_details );
							$cell->id = 'student-'.$activestudent->id.'_section-'.$sectionnumber;
							$cell->attributes ['class'] = 'section_results '.$cell_class.' center section_col_left section_col_right student_' . $activestudent->id;
							$cell->colspan = count($unit[$sectionnumber]);
							$row->cells [] = $cell;
						}
					}
					$sectionnumber ++;
				}
				// Set row formatting
				$row->attributes ['class'] = 'student_' . $activestudent->id;
				if ($atrisk > 0) {
					if ($failing > 0) {
						$row->attributes ['class'] .= ' atrisk';
					} else {
						$row->attributes ['class'] .= ' atrisk';
					}
				} else {
					$row->attributes ['class'] .= ' notflagged';
				}
				$table->data [] = $row;
			} else {
				$suspendedcount ++;
			}
		}
		$activestudentcount = $studentcount - $suspendedcount;
		if ($activestudentcount == 1) {
			$activestudentdisplay = $activestudentcount . " active student";
		} else {
			$activestudentdisplay = $activestudentcount . " active students";
		}
		if ($suspendedcount == 1) {
			$activestudentdisplay .= ", " . $suspendedcount . " suspended student";
		} elseif ($suspendedcount > 1) {
			$activestudentdisplay .= ", " . $suspendedcount . " suspended students";
		}
		if ($facultycount > 0) {
			$activestudentdisplay .= ", " . $facultycount . " faculty";
		}
	}
	$displaycount = $studentcount - $suspendedcount;
} else {
	$results = "No users enrolled.";
	$activestudentdisplay = "No students enrolled";
}

if ($sort == 'firstname' or $sort == 'lastname') {
	$orderby = "u.$sort $dir";
} else {
	$orderby = "";
}
$mainuserfields = user_picture::fields ( 'u' );

$baseurl = new moodle_url ( 'index.php', array (
		'id' => $COURSE->id,
		'sort' => $sort,
		'dir' => $dir,
		'perpage' => $perpage 
) );

// Display the results
echo $OUTPUT->header ();
echo $OUTPUT->heading ( 'ALPHA - '. get_string ( 'pagetitle', 'report_cuprogress' ) . ' for ' . $COURSE->shortname );
echo html_writer::tag ( 'h4', $coursestartdate, array (
		'id' => 'coursestartdate',
		'class' => 'legend',
		'title' => 'Estimated Course End Date: '. date_format_string ( $COURSE->enddate, '%A %B %e, %Y', $USER->timezone)
) );
echo html_writer::link('#',"Show only potentially at-risk students",array('id'=>'atriskstudentsbutton','class'=>'button'));
echo html_writer::link('#',"Show all students",array('id'=>'allstudentsbutton','style'=>'display: none;','class'=>'button'));
echo html_writer::link('#','Show all assignment results',array('id'=>'toggleassignmentresults','class'=>'button','data-visibility'=>'hidden'));
//echo report_cuprogress_output_action_buttons ( $id, $group, $querystartdate, $queryenddate, 99, $PAGE->url );
echo $OUTPUT->paging_bar ( $displaycount, $page, $perpage, $baseurl );
echo html_writer::table ( $table );
echo $results;
echo "<hr/>";
echo "<div id='enrolled_user_count'>Enrolled users: " . $enrolledcount . "  (" . $activestudentdisplay . ")</div>";
echo html_writer::link ( new moodle_url ( $CFG->wwwroot . '/enrol/users.php?id=' . $COURSE->id ), 'View Enrollments', array (
		'class' => 'button pull-left' 
) );
echo html_writer::link ( new moodle_url ( $CFG->wwwroot . '/grade/report/grader/index.php?id=' . $COURSE->id ), 'View Gradebook', array (
		'class' => 'button pull-left' 
) );
echo html_writer::tag('div','C = Complete but awaiting grade<br/>I = Incomplete<br/>L = Submitted late and awaiting grade<br/>S = Submitted on-time but awaiting grade<br/>U = Status unkown<br/>?? Marked complete by student', array( 'style' => 'clear: both; font-style: italics; padding-top: 16px;' ));
echo $OUTPUT->footer ();
